/**
 * If the opposite is not said, for this application, orientation is the angle to the North according to the clockwise direction
 */

class Point{
	/**  private member */
	//#x = 0;
	x = 0;
	/**  private member */
	//#y = 0;
	y = 0;
	/**  private member */
	//#z = 0;
	z = 0;
	/**  private static member */
	//static #orientation = 0;
	static orientation = 0;
	/**
	 * constructs a new  point
	 * @param mode iIndicates the construction mode as explained in the rest of parameters:
	 * 
	 * @param arg1 If mode == "p" arg 1 represents the absolute x
	 * 		 if mode == "r" arg1 represents the point from which the other arguments will be referenced.
	 * @param arg2 If mode == "p" arg2 represents the absolute y.
	 * 		 if mode = "r" arg 2 arg1 represents the distance in a straight line from arg1 to the point to be created
	 * 
	 * @param arg3 if mode == "p" represents the absolute z.
	 * 		 if mode = "r" is the orientation of the straight line that joins the point indicated in arg1 and the new point to be created
	 * 
	 * @param arg4 Valid only for mode r, indicates de z postion of rferenced point. If stric equal to  "-1"(derfault) 
	 * 	z is the same as referenced point   
	 */
	constructor(mode = "p", arg1 = 0, arg2 = 0, arg3 = 0, arg4 = -1){
		if (mode == "p"){
		/*
			this.#x = Number(arg1);
			this.#y = Number(arg2);
			this.#z = Number(arg3);
		*/
			this.x = Number(arg1);
			this.y = Number(arg2);
			this.z = Number(arg3);
			return;
		}
		if (mode == "r"){
			ang = arg3 - Point.orientation;
		/*
			this.#x = arg1.#x + (arg2 * Math.sin(ang * (Math.PI/180)));  
			this.#y = arg1.#y - (arg2 * Math.cos(ang * (Math.PI/180)));
			if (arg3 === "-1"){
				this.#z = arg1.#z;	
			}else{
				this.#z = Number(arg4);
			}
		*/
			this.x = arg1.x + (arg2 * Math.sin(ang * (Math.PI/180)));  
			this.y = arg1.y - (arg2 * Math.cos(ang * (Math.PI/180)));
			if (arg3 === "-1"){
				this.z = arg1.z;	
			}else{
				this.z = Number(arg4);
			}
			return;
		}
 		throw "parameter mode error";
	}
	static get orientation(){
		returns this.orientation;
	}
	static set orientation(orientation){
		this.orientation = orientation;
	}
	get x(){
		return this.x;
	}
	get y(){
		return this.y;
	}
	get z(){
		return this.z;
	}
	set x(x){
		this.x = x;
	}
	set y(y){
		this.y = y;
	}
	set z(z){
		this.z = z;
	}

}

class paredRapida{
	points = [new Point(),new Point(),new Point(),new Point()];
	paretInici = null;
	paretName = "";
	paret = null;
	curve = false;
	endpoint= null;
	endWall = null;
	controlPoint = null;
	defaultControlP = false;
    defaultControlPO = false;

	constructor(arg1 = null){
		this.paretInici = paretInicial;
		/* read from xml */
		if (arg != null && typeof arg1 == "object" && !Array.isArray(arg)){
			try{ 
				if (arg1.tagName == "StartingWall"){
					this.paretInici = null;
				}else if (arg1.tagName == "walls"){
					this.paretInici= {name:"",length: 0,orientacio: 0};
					this.paretInici.name = arg1.getAttribute("originwall");
					this.paretInici.length = Number(arg1.getAttribute("distance"));
					this.paretInici.orientation = Number(arg1.getAttribute("oritentation"));
				}else{
					throw "Malformed xml: Tag name error";	
				}
				this.paretName = xmlNode.getAttribute("id");
			}catch(error){
				throw "pareRapida Constructor: " + error;
			}
			try{
				ele = arg1.getElementByTagName("startingwallpoint")[0];
				this.paret = {length: ele.getAttribute("length"), orientation: ele.getAttribute("orientation")};
			}catch(error){
				throw "Starting Wall:" + error ;
			}
			try{
				ele = arg1.getElementByTagName("curve")[0];
				curve = this.getElement(ele);
				curve= curve == "true" || curve == 1 ? true : false; 
			}catch(error){
				throw "Curve: " + error ;
			}
			if (curve){
				try{
					ele = arg1.getElementByTagName("endPoint")[0];
					this.endpoint = {length: ele.getAttribute("length"), orientation: ele.getAttribute("orientation")};
					ele = arg1.getElementByTagName("endWall")[0];
					this.endWall = {length: ele.getAttribute("length"), orientation: ele.getAttribute("orientation")};
					ele = arg1.getElementByTagName("controlPoint");
					if (ele.length > 0){
						this.controlPoint = {length: ele[0].getAttribute("length"), orientation: ele[0].getAttribute("orientation")};
						if (this.controlPoint.orientation == null){
							this.controlPoint.orientation = this.paretInici.orientation;
							this.defaultControlPO = true;
						}
					}else{
						let ang1 = Math.abs(this.paret.orientation - this.endpoint.orientation);
						let ang2 = Math.abs(this.endWall.orientation - this.endpoint.orientation);
						let l = prolong(this.endpoint.length,ang1,ang2);
						this.controlPoint = {length:l,orientation:this.paret.orientation);
						this.defaultControlP = true;
					}
				}catch(error){
					throw "Malformed Curve: " + error ;
				}
			}
			

		}
	}
	static prolong(l1,a1,a2){
		return li /(Math.cosd(a1) + Math.sind(a1) * Math.cotd(a2));
	}
	static getElement(x){
		return x.childNodes[0].nodeValue;		
	}
}

