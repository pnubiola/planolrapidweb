import math

diam = 350
agulla = 35
cx = 175
cy = 175 
r1 = 175 - 19
r2 = 175 - 9
r3 = 175 - 4
f = open("compass.txt", "w")
for i in range(0 ,360):
    a1 = (450 - i) % 360
    a = math.radians(a1)
    x1 = cx + math.cos(a) * r1
    y1 = cy - math.sin(a) * r1
    if ( i % 10 == 0):
        x2 = cx + math.cos(a) * 175
        y2 = cy - math.sin(a) * 175
    elif ( i % 5 == 0):
        x2 = cx + math.cos(a) * r3
        y2 = cy - math.sin(a) * r3
    else:
        x2 = cx + math.cos(a) * r2
        y2 = cy - math.sin(a) * r2
    f.write("<line class=\"guiesRosaDelsVentes\" x1=\"" + str(round(x1,2)) + "\" y1=\"" + str(round(y1,2)) + "\" x2=\"" + str(round(x2,2)) + 
		"\" y2=\"" + str(round(y2,2)) + "\"/>\n")

    if ( i % 10 == 0):
        f.write("<text x=\"175\" y=\"28\" class=\"TextCompass\" "
			"transform=\"rotate(" + str(i) + " " + str(cx) + " " + str(cy) + ")\">" + str(i) + "</text>\n")

f.close()
