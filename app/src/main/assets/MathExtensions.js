/**
 * adds extension to math library
 */

 /**
  * convert an angle from degrees to radiants
  * @param alfa angle in degrees
  * @return alfa converted to radiants
  */
 Math.radiants = function (alfa){
	return (alfa * Math.PI/180);
}

/**
 * convert an angle from radiants to degreess
 * @param alfa angle in radiants
 * @return alfa converted to degreess
 */
Math.degrees = function (alfa){
	return (alfa * 180/Math.PI);
}

/**
 * returns the cotangent of an angle
 * @param alfa the angle in radiants
 * return the cotangent
 */
Math.cot = function (alfa){
	return 1 / Math.tan(alfa);
}

/**
 * returns the sinus of an angle
 * @param alfa the angle in degrees
 * return the sinus
 */
Math.sind = function (alfa){
	return Math.sin(alfa * Math.PI/180);
}

/**
 * returns the cosinus of an angle
 * @param alfa the angle in degrees
 * return the cosinus
 */
Math.cosd = function (alfa){
	return Math.cos(alfa * Math.PI/180);
}

/**
 * returns the tangent of an angle
 * @param alfa the angle in degrees
 * return the tangent
 */
Math.tand = function (alfa){
	return Math.tan(alfa * Math.PI/180);
}

/**
 * returns the cotangent of an angle
 * @param alfa the angle in degrees
 * return the cotangent
 */
Math.cotd = function (alfa){
	return 1 / Math.tan(alfa * Math.PI/180);
}
