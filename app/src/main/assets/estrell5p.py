import math

px0 = 175;
py0 = 115;
r = 25;
ra = 1.618
ll = 2 * r * math.cos(math.radians(18))
b = ll / ra
c = ll / math.pow(ra,2)
f = open("compass.txt", "w")
py1b = py0 + c * math.cos(math.radians(18))
py1 = str(round(py1b,2)) 
prx1b = px0 + c * math.sin(math.radians(18))
plx1b = px0 - c * math.sin(math.radians(18))
prx1 = str(round(prx1b,2))
plx1 = str(round(plx1b,2)) 
prx2 = str(round(prx1b + c,2))
plx2 = str(round(plx1b - c,2))
py3 = str(round(py0 + b * math.cos(math.radians(18)),2))
prx3 = str(round(px0 + b* math.sin(math.radians(18)),2))
plx3 = str(round(px0 - b * math.sin(math.radians(18)),2))
py4 = str(round(py0 + ll * math.cos(math.radians(18)),2))
prx4 = str(round(px0 + ll * math.sin(math.radians(18)),2))
plx4 = str(round(px0 - ll * math.sin(math.radians(18)),2))
px5 = str(px0)
py5 = str(round(py1b + b* math.sin(math.radians(36)),2)) 

f.write("<polygon points=\""+ str(px0) + "," + str(py0) + " " + prx1 + "," + py1 + " " + prx2 + "," + py1 + " " + prx3 + ","
	+ py3 + " " + prx4	+ ","+ py4 + " " + px5 + "," + py5 + " " + plx4 + "," + py4 + " " + plx3 + "," +py3 + " "
	+ plx2 + "," + py1 + " " + plx1+ "," + py1 + "\" fill=\"white\" stroke=\"none\"/>\n")
f.close()

